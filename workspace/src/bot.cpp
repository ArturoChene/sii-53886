#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>




int main(){
	int fd;
	DatosMemCompartida *pdatosComp;
	fd=open("/tmp/datosComp",O_RDWR);
	if (fd < 0) {
           perror("Error en la apertura del fichero");
           return 1;
        }
        
        pdatosComp=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
        
	if (pdatosComp==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return 1;
	}
	if (close(fd)<0) {
	perror("No puede cerrarse el descriptor de datos compartidos");
	return 1;
	}
	
	//while(pdatosComp!=NULL)
	while(1){
		if(pdatosComp->esfera.centro.y < ((pdatosComp->raqueta1.y1+pdatosComp->raqueta1.y2)/2)){
			pdatosComp->accion=-1;
		}
		else if(pdatosComp->esfera.centro.y == ((pdatosComp->raqueta1.y1+pdatosComp->raqueta1.y2)/2)){
			pdatosComp->accion=0;
		}
		else if(pdatosComp->esfera.centro.y > ((pdatosComp->raqueta1.y1+pdatosComp->raqueta1.y2)/2)){
			pdatosComp->accion=1;
		}
			
		usleep(25000);
}	
	munmap(pdatosComp, sizeof(DatosMemCompartida));
	int ul;
	ul=unlink("/tmp/datosComp");
	if (ul<0) {
		perror("No puede cerrarse el FIFO del bot");
		return 1;
	}
	
	printf("Bot teminado\n");
	return 1;
}

