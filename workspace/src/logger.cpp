#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include "Puntos.h"

int main(){
int fd;
int rd;
int cl;
int ul;
Puntos puntos;

// crea el FIFO
if (mkfifo("/tmp/FIFO_LOGGER", 0666)<0) {
	perror("No puede crearse el FIFO_LOGGER");
	return 1;
}

// Abre el FIFO
fd=open("/tmp/FIFO_LOGGER", O_RDONLY);
if (fd<0) {
	perror("No puede abrirse el FIFO_LOGGER");
	return 1;
}

//añadido control de errores
//rd=read(fd, &puntos, sizeof(puntos));
while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
	/*if (read(fd, &puntos, sizeof(puntos))<0) {
	perror("No puede leerse el FIFO_LOGGER");
	return 1;
	}*/

	if(puntos.ult_ganador==1){
		printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
	}
	else if(puntos.ult_ganador==2){
		printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
	}
	else if(read(fd, &puntos, sizeof(puntos))==0) {
		printf("El juego ha acabado.\n");
		return 0;
	}

}

cl=close(fd);
if (cl<0) {
	perror("No puede cerrarse el descriptor del FIFO_LOGGER");
	return 1;
}

//cierra el FIFO
ul=unlink("/tmp/FIFO_LOGGER");
if (ul<0) {
	perror("No puede cerrarse el FIFO_LOGGER");
	return 1;
}

}
