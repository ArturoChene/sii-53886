// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
#include <pthread.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	//munmap(pdatosComp, sizeof(pdatosComp));
	//unlink("datosComp");
	close (fd);
	unlink("/tmp/FIFO_LOGGER");
	
	close(FIFO_SERVIDOR_CLIENTE);
	unlink("/tmp/FIFO_SERVIDOR_CLIENTE");
	
	close(FIFO_CLIENTE_SERVIDOR);
	unlink("/tmp/FIFO_CLIENTE_SERVIDOR");
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	if(esfera.radio<=0.4 || esfera.radio >=2)
		esfera.pulso = -esfera.pulso;
	esfera.radio += esfera.pulso * 0.025f;

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.ult_ganador = 2; //jugador 2 hace 1 punto
		write(fd, &puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.ult_ganador = 1; //jugador 1 hace 1 punto
		write(fd, &puntos, sizeof(puntos));
	}
	if (puntos2 == 3 || puntos1 == 3)
	{
		if (puntos1 == 3)
		{
			printf("Jugador 1 gana\n");
		}
		else if (puntos2 == 3)
		{
			printf("Jugador 2 gana\n");
		}

		exit(1);
	}
	//pdatosComp -> esfera = esfera;
	//pdatosComp -> raqueta1 = jugador1;

	/*if (pdatosComp->accion == -1)
	OnKeyboardDown('s',0,0);
	else if (pdatosComp->accion == 0){}
	else if (pdatosComp->accion == 1)
	OnKeyboardDown('w',0,0);*/
	
	//SE REGISTRAN LOS DATOS A ENVIAR EN UNA CADENA Y SE ESCRIBE EN EL FIFO 		LA MISMA
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f",
		esfera.centro.x,esfera.centro.y, 
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2, esfera.radio);
	write(FIFO_SERVIDOR_CLIENTE, cad, sizeof(cad));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	//SE COMENTA ESTO PUES EL SERVIDOR LEE LOS DATOS DE LA 	TUBERÍA PARA ACTUALIZAR LAS VARIABLES DE KEY
	/*switch(key)
	{
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad	.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
	
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     FIFO_CLIENTE_SERVIDOR=open("/tmp/FIFO_CLIENTE_SERVIDOR",O_RDONLY);
	if(FIFO_CLIENTE_SERVIDOR<0) perror("No puede abrirse el FIFO_CLIENTE_SERVIDOR");
		//return 1;
     while (1) {
            usleep(10);
            char cad[100];
            read(FIFO_CLIENTE_SERVIDOR, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundoServidor::Init()
{
	fd = open("/tmp/FIFO_LOGGER", O_WRONLY);
	if(fd<0)
	{
		perror("Error abriendo el FIFO_LOGGER");
		//return 1;
	}
	
	//SE ABRE EL FIFO DEL SERVIDOR AL CLIENTE
	FIFO_SERVIDOR_CLIENTE=open("/tmp/FIFO_SERVIDOR_CLIENTE", O_WRONLY);
	if(FIFO_SERVIDOR_CLIENTE<0){
		perror("No puede abrirse el FIFO_SERVIDOR_CLIENTE");
		//return 1;
	}
	
	//EL SERVIDOR TAMPOCO ESCRIBE EN DATOSCOMP NI PROYECTA 	NADA
	/*
	int fd_mmap;
	fd_mmap = open("datosComp", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0)
	{
		perror("Error creando fichero");
		return;
	}
	datosComp.esfera = esfera;
	datosComp.raqueta1=jugador1;
	datosComp.accion = 0;
	int wr;
	wr = write(fd_mmap, &datosComp, sizeof(datosComp));
	if (wr<0)
	{
		perror("Error de escritura");
		return;
	}
	pdatosComp = static_cast <DatosMemCompartida*>(mmap(NULL, 		sizeof(datosComp), PROT_READ|PROT_WRITE,MAP_SHARED, fd_mmap, 0));
	if(pdatosComp == MAP_FAILED)
	{
		perror("Error proyectando en memoria");
		close(fd_mmap);
		return;
	}
	close (fd_mmap);
	*/
	
	//CREACIÓN DEL THREAD
	//pthread_attr_init(&atrib);
	//pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, NULL, hilo_comandos, this);

	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
