// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

//ARTURO CHENE-MILLEVILLE PEREZ
//ARCHIVO MODIFICADO

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
//#include "DatosMemCompartida.h"
#include "Puntos.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	Puntos puntos;
	int fd;
	//DatosMemCompartida datosComp;
	//DatosMemCompartida *pdatosComp;

	int puntos1;
	int puntos2;
	
	int FIFO_SERVIDOR_CLIENTE;
	int FIFO_CLIENTE_SERVIDOR;
	
	pthread_t thid1;
	pthread_attr_t atrib;
};

#endif // !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
